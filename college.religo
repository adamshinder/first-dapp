type applicant = {
name : string, 
satscore : int, 
gpa : int 
}; // creates a record of applicants

//const collegeBoard : address = ("tz1SVKrTXr7UPRZZKQY3Gudx1QWs1BKaDpkF": address);

type enter_record =
| Addname (string)
| Addsatscore (int)
| Addgpa (int)
//| Submit (int) // is this supposed to be int? 

let addname = ((n, name): (applicant, string)) : applicant => {...n, name: name}; 
let addsatscore = ((s, score): (applicant, int)) : applicant => {...s, satscore: score}; 
let addgpa = ((s, gpa): (applicant, int)) : applicant => {...s, gpa: gpa}; 
//let changeMessage = ((s, msg): (storage, string)): storage => {...s, message: msg}; 
//^^^ makes an instance variable called msg and sets that to the message when executed ^^^
 
let main = ((p,applicant): (enter_record, applicant)) => {
  let applicant =
    switch (p) {
        | Addname (t) => addname((applicant, t))
        | Addsatscore (t) => addsatscore((applicant, t))
        | Addgpa (t) => addgpa((applicant,t))
      //  | Submit (t)
    };
  ([]: list(operation), applicant); //returns a list of all the applicants
};
